<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model {
	protected $guarded = ['id', 'created_at', 'updated_at', ];


    public function permits() {
        return $this->hasMany('App\Permit');
    }


}
