<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model {
    protected $guarded = ['id', 'created_at', 'updated_at', ];

    public function permits() {
        return $this->belongsTo('App\Permit', 'permits_id');
    }


}
