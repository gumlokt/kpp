<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model {
    protected $guarded = ['id', 'created_at', 'updated_at', ];


    public function emitters() {
        return $this->belongsTo('App\Emitter', 'emitters_id');
    }

    public function owners() {
        return $this->belongsTo('App\Owner', 'owners_id');
    }

    public function cars() {
        return $this->belongsTo('App\Car', 'cars_id');
    }


    public function logs() {
        return $this->hasMany('App\Log');
    }





}
