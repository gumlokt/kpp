<?php

namespace App\Http\Controllers;


use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;


use Illuminate\Http\Request;
use Session;
use DB;

use App\Car;
use App\Permit;

use App\Log;

ini_set('max_execution_time', 300); // longer script execution timeout for exporting logs to Excel

class LogController extends Controller
{

    public function summary()
    {
        $summary['total'] = Log::count();
        $summary['in'] = Log::where('direction', '=', 'in')->count();
        $summary['out'] = $summary['total'] - $summary['in'];

        return $summary;
    }



    public function fieldAutocomplete(Request $request)
    {
        $params = $request->input('params');

        $field = $params['field'];
        $value = $params['value'];

        $selection = Log::select($field)
            ->where($field, '!=', '')
            ->where(function ($query) use ($field, $value) {
                $query->where($field, 'LIKE', $value . '%');
            })
            ->distinct()
            ->get();


        $result = [];
        foreach ($selection as $entry) {
            $result[] = $entry->$field;
        }


        return $result;
        return json_encode($result);
    }










    public function lastLogs(Request $request)
    {
        $export  = $request->input('export');
        $page    = $request->input('page');
        $rows    = $request->input('rows');

        $offset  = ($page - 1) * $rows;

        $logs = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                'logs.id',
                'logs.direction',
                'logs.driver',
                'logs.cargo',
                'logs.customer',
                'logs.destination',
                'logs.comment',
                'logs.created_at',
                'permits.number',
                'permits.weapon',
                'permits.privileged',
                'emitters.emitter',
                'owners.owner',
                'cars.plate',
                'cars.model'
            )
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->when($export, function ($query) {
                return $query->orderBy('logs.created_at', 'ASC');
            })
            ->when(!$export, function ($query) {
                return $query->orderBy('logs.created_at', 'DESC');
            })
            ->get();


        if ($export) {
            $border = (new BorderBuilder())
                ->setBorderTop(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderRight(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderBottom(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderLeft(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->build();

            $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->build();

            $styleHeader = (new StyleBuilder())
                ->setBorder($border)
                ->setFontBold()
                // ->setShouldWrapText()
                ->setFontColor(Color::BLUE)
                ->setBackgroundColor(Color::YELLOW)
                ->build();

            $styleRow = (new StyleBuilder())
                ->setBorder($border)
                ->build();

            $writer = WriterFactory::create(Type::ODS);
            $writer->setShouldCreateNewSheetsAutomatically(true);
            $writer->setDefaultRowStyle($defaultStyle)->openToBrowser('export.ods');

            $writer->addRowWithStyle([
                'Направление',
                'Гос. номер',
                'Марка авто',
                'Владелец авто',
                'Номер пропуска',
                'Эмитент',
                'Дата и время',
                'Водитель',
                'Груз',
                'Заказчик',
                'Пункт назначения',
                'Разрешение на оружие',
                'Без досмотра',
                'Примечание',
            ], $styleHeader);

            foreach ($logs as $item) {
                $writer->addRowWithStyle([
                    $item->direction,
                    $item->plate,
                    $item->model,
                    $item->owner,
                    (int)$item->number,
                    $item->emitter,
                    $item->created_at,
                    $item->driver,
                    $item->cargo,
                    $item->customer,
                    $item->destination,
                    $item->weapon ? 'Да' : '',
                    $item->privileged ? 'Да' : '',
                    $item->comment,
                ], $styleRow); // add a row at a time
            }

            $writer->close();


            return $writer->getSheets();
        }


        $totalRows = DB::table('logs')->count();

        return ['totalRows' => $totalRows, 'logs' => $logs];
    }















    public function plateLogs(Request $request)
    {
        $export  = $request->input('export');
        $plate   = $request->input('plate');
        $page    = $request->input('page');
        $rows    = $request->input('rows');

        $offset  = ($page - 1) * $rows;

        $logs = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                'logs.id',
                'logs.direction',
                'logs.driver',
                'logs.cargo',
                'logs.customer',
                'logs.destination',
                'logs.comment',
                'logs.created_at',
                'permits.number',
                'permits.weapon',
                'permits.privileged',
                'emitters.emitter',
                'owners.owner',
                'cars.plate',
                'cars.model'
            )
            ->where('cars.plate', '=', $plate)
            ->orderBy('logs.created_at', 'DESC')
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        if ($export) {
            $border = (new BorderBuilder())
                ->setBorderTop(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderRight(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderBottom(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderLeft(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->build();

            $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->build();

            $styleHeader = (new StyleBuilder())
                ->setBorder($border)
                ->setFontBold()
                // ->setShouldWrapText()
                ->setFontColor(Color::BLUE)
                ->setBackgroundColor(Color::YELLOW)
                ->build();

            $styleRow = (new StyleBuilder())
                ->setBorder($border)
                ->build();

            $writer = WriterFactory::create(Type::ODS);
            $writer->setShouldCreateNewSheetsAutomatically(true);
            $writer->setDefaultRowStyle($defaultStyle)->openToBrowser('export.ods');

            $writer->addRowWithStyle([
                'Направление',
                'Гос. номер',
                'Марка авто',
                'Владелец авто',
                'Номер пропуска',
                'Эмитент',
                'Дата и время',
                'Водитель',
                'Груз',
                'Заказчик',
                'Пункт назначения',
                'Разрешение на оружие',
                'Без досмотра',
                'Примечание',
            ], $styleHeader);

            foreach ($logs as $item) {
                $writer->addRowWithStyle([
                    $item->direction,
                    $item->plate,
                    $item->model,
                    $item->owner,
                    (int)$item->number,
                    $item->emitter,
                    $item->created_at,
                    $item->driver,
                    $item->cargo,
                    $item->customer,
                    $item->destination,
                    $item->weapon ? 'Да' : '',
                    $item->privileged ? 'Да' : '',
                    $item->comment,
                ], $styleRow); // add a row at a time
            }

            $writer->close();


            return $writer->getSheets();
        }

        $totalRows = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                'logs.id',
                'logs.direction',
                'logs.driver',
                'logs.cargo',
                'logs.customer',
                'logs.destination',
                'logs.comment',
                'logs.created_at',
                'permits.number',
                'emitters.emitter',
                'owners.owner',
                'cars.plate',
                'cars.model'
            )
            ->where('cars.plate', '=', $plate)
            ->count();


        return ['totalRows' => $totalRows, 'logs' => $logs];
    }






    // register event
    public function register(Request $request, $direction)
    {
        $now = time();
        $lastEntry = Log::orderBy('created_at', 'DESC')->first();

        if (!empty($lastEntry) and $now < strtotime($lastEntry->created_at)) {
            return ['error' => true, 'message' => 'Произошла ошибка! Время последней записи в журнале - ' . $lastEntry->created_at . ' превышает текущее время - ' . date('Y-m-d H:i:s') . '. Возможно, на компьютере не выставлены правильные дата и время...'];
        }

        $this->validate($request, [
            'permitId'      => 'required',
            'driver'        => 'required',
            'cargo'         => 'required',
            'customer'      => 'required',
            'destination'   => 'required',
        ]);

        $sql = [
            'direction'     => $direction,
            'permits_id'    => $request->input('permitId'),
            'driver'        => $request->input('driver'),
            'cargo'         => $request->input('cargo'),
            'customer'      => $request->input('customer'),
            'destination'   => $request->input('destination'),
            'comment'       => $request->input('comment'),
        ];

        $log  = Log::create($sql);


        return 'All data was successfully saved...';
    }




    // inverse in/out direction
    public function reverse(Request $request, $id)
    {
        $log = Log::find($id);

        if ('in' == $log->direction) {
            $log->direction = 'out';
        } else {
            $log->direction = 'in';
        }

        if ($log->save()) {
            return ['error' => false, 'message' => 'Reversing was completed successfully...'];
        } else {
            return ['error' => true, 'message' => 'Reversing has not performed...'];
        }
    }
}
