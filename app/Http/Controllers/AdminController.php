<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\User;

use App\Emitter;
use App\Owner;
use App\Car;
use App\Permit;
use App\Log;

use Artisan;


class AdminController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $stats['cars'] = Car::count();
        $stats['owners'] = Owner::count();

        $stats['totalLogs'] = Log::count();
        $stats['inLogs'] = Log::where('direction', '=', 'in')->count();

        $stats['firstLog'] = Log::first();
        $stats['lastLog'] = Log::orderBy('id', 'DESC')->first();

        $stats['totalPermits'] = Permit::count();
        $stats['validPermits'] = Permit::where('end', '>', date('Y-m-d H:i:s'))->count();

        $stats['totalEmitters'] = Emitter::count();
        $stats['validEmitters'] = Emitter::where('actual', '=', 1)->count();


        return view('adminpanel.index', ['stats' => $stats]);
    }


    // show clearing options
    public function showClearing() {
        return view('adminpanel.clearing.index');
    }



    // apply selected clearing options
    public function cleardb(Request $request) {
        $date =  date_format(date_create_from_format('d.m.Y H:i:s', $request->input('date') . ' 23:59:59'), 'Y-m-d H:i:s');
        $currentTime = date('Y-m-d H:i:s');
        if ($date > $currentTime) {
            return [ 'message' => 'Очистка базы не произведена, т. к. вы задали дату, превышающую текущую.' ];
        }

        $permitsToDelete = Permit::where('end', '<=', $date)->get();
        $deletedPermits = Permit::where('end', '<=', $date)->delete();
        
        $permit_ids = [];
        $owner_ids = [];
        $car_ids = [];
        foreach ($permitsToDelete as $permit) {
            $permit_ids[] = $permit->id;

            if(!in_array($permit->owners->id, $owner_ids)){
                $owner_ids[] = $permit->owners->id;
            }
            if(!in_array($permit->cars->id, $car_ids)){
                $car_ids[] = $permit->cars->id;
            }
        }

        sort($permit_ids);
        sort($owner_ids);
        sort($car_ids);


        // Clear Log table
        foreach ($permit_ids as $permit_id) {
            Log::where('permits_id', '=', $permit_id)->delete();
        }    

        // Clear Owner table
        $ownersToDelte_ids = [];
        foreach ($owner_ids as $owner_id) {
            $permits = Permit::where('owners_id', '=', $owner_id)->count();

            if (!$permits) {
                $ownersToDelte_ids[] = $owner_id;
            }
        }
        if (count($ownersToDelte_ids)) {
            Owner::destroy($ownersToDelte_ids);
        }

        // Clear Car table
        $carsToDelte_ids = [];
        foreach ($car_ids as $car_id) {
            $permits = Permit::where('cars_id', '=', $car_id)->count();

            if (!$permits) {
                $carsToDelte_ids[] = $car_id;
            }
        }
        if (count($carsToDelte_ids)) {
            Car::destroy($carsToDelte_ids);
        }


        return [ 'message' => 'Очистка базы данных завершена успешно. Удалено пропусков: ' . count($permit_ids) ];
    }

    // show backup page
    public function showBackups() {
        return view('adminpanel.backups.index');
    }

    // make DB backup
    public function dumpdb() {
        sleep(1);
        Artisan::call('backup:run --only-db');

        return [ 'message' => 'Бэкап базы данных создан успешно. Сохраните его на свой носитель.' ];
    }

    // return file list
    public function displayFiles() {
        $paths = Storage::files('http---localhost');
        $files = [];
        
        foreach ($paths as $path) {
            $files[] = ltrim(strstr($path, '/'), '/');
        }
        rsort($files);

        return [ 'files' => $files ];
    }

    // download selected file
    public function downloadFile($fileName) {
        // $fileName = $request->input('fileName');

        return Storage::download('http---localhost/' . $fileName);
    }

    // delete selected file
    public function deleteFile(Request $request) {
        sleep(1);
        $fileName = $request->input('fileName');
        Storage::delete('http---localhost/' . $fileName);

        return [ 'message' => 'Выбранный бэкап базы данных успешно удалён.' ];
    }




}
