<?php

namespace App\Http\Controllers;

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;


use Illuminate\Http\Request;

use DB;

use App\Setting;

use App\Emitter;
use App\Owner;
use App\Car;

use App\Permit;
use App\Log;

ini_set('max_execution_time', 300); // longer script execution timeout for exporting logs to Excel

class AjaxController extends Controller {

    public function actualEmitters(Request $request) {
        $emitters = Emitter::where('actual', '=', 1)->orderBy('id', 'ASC')->get();

        return json_encode($emitters);
    }





    public function filterPermits(Request $request) {
        $filters = $request->input('filters');
        $page = $request->input('page');
        $rows = $request->input('rows');
        $offset = ($page - 1) * $rows;


        $categories     = $filters['categories'];
        $emitters       = $filters['emitters'];

        $plate          = $filters['plate'];
        $model          = $filters['model'];
        $owner          = $filters['owner'];

        $number         = $filters['number'];
        $start          = '';
        $end            = '';


        if (!empty($filters['start'])) {
            $start =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['start'] . ' 00:00:00'), 'Y-m-d H:i:s');
        }
        if (!empty($filters['end'])) {
            $end =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['end'] . ' 23:59:59'), 'Y-m-d H:i:s');
        }


        // Firstly, save selected filtering settings to DB
        $setups = [
            'categories'   => count($categories) > 0 ? serialize($categories) : '',
            'emitters'     => count($emitters) > 0 ? serialize($emitters) : '',
            'plate'        => !empty($plate) ? $plate : '',
            'model'        => !empty($model) ? $model : '',
            'owner'        => !empty($owner) ? $owner : '',
            'number'       => !empty($number) ? $number : '',
            'start'        => $start,
            'end'          => $end,
         ];

        foreach ($setups as $parameter => $value) {
            $setup = Setting::where('profile', '=', 'permits')->where('parameter', '=', $parameter)->first();

            if(!empty($setup)) {
                $setup->update([ 'value' => $value ]);
            } else {
                $setup = Setting::create([ 'profile' => 'permits', 'parameter' => $parameter, 'value' => $value ]);
            }
        }


        // Secondly, search and return requested permits
        $totalRows = DB::table('permits')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->select('emitters.emitter', 'emitters.abbr', 'owners.owner', 'cars.model', 'cars.plate', 'permits.id', 'permits.number', 'permits.start', 'permits.end')
            ->when($categories, function ($query, $categories) {
                if (count($categories) == 1) {
                    $now = date('Y-m-d H:i:s');

                    if ('valid' == $categories[0]) {
                        return $query->where('end', '>=', $now);
                    } else {
                        return $query->where('end', '<=', $now);
                    }
                }
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($number, function ($query, $number) {
                return $query->where('number', 'LIKE', '%' . $number . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('start', '=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('end', '=', $end);
            })
            ->count();


        $permits = DB::table('permits')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->select('emitters.emitter', 'emitters.abbr', 'owners.owner', 'cars.model', 'cars.plate', 'permits.id', 'permits.number', 'permits.start', 'permits.end')
            ->when($categories, function ($query, $categories) {
                if (count($categories) == 1) {
                    $now = date('Y-m-d H:i:s');

                    if ('valid' == $categories[0]) {
                        return $query->where('end', '>=', $now);
                    } else {
                        return $query->where('end', '<=', $now);
                    }
                }
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($number, function ($query, $number) {
                return $query->where('number', 'LIKE', '%' . $number . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('start', '=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('end', '=', $end);
            })
            ->orderBy('permits.end', 'DESC')
            ->orderBy('permits.start', 'ASC')
            ->orderBy('permits.number', 'DESC')
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        return [ 'totalRows' => $totalRows, 'permits' => $permits ];
    }

















    public function filterSearch(Request $request) {
        $export  = $request->input('export');
        $filters = $request->input('plate');
        $page    = $request->input('page');
        $rows    = $request->input('rows');


        $offset         = ($page - 1) * $rows;

        $direction      = $filters['direction'];
        $emitters       = $filters['emitters'];
        $plate          = $filters['plate'];
        $model          = $filters['model'];
        $owner          = $filters['owner'];

        $customer       = $filters['customer'];
        $driver         = $filters['driver'];
        $destination    = $filters['destination'];
        $cargo          = $filters['cargo'];

        $start = '';
        $end = '';

        if (!empty($filters['start'])) {
            $start =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['start'] . ':00'), 'Y-m-d H:i:s');
        }
        if (!empty($filters['end'])) {
            $end =  date_format(date_create_from_format('d.m.Y H:i:s', $filters['end'] . ':00'), 'Y-m-d H:i:s');
        }


        // Firstly, save selected filtering settings to DB
        $setups = [
            'direction'    => count($direction) > 0 ? serialize($direction) : '',
            'emitters'     => count($emitters) > 0 ? serialize($emitters) : '',
            'plate'        => !empty($plate) ? $plate : '',
            'model'        => !empty($model) ? $model : '',
            'owner'        => !empty($owner) ? $owner : '',
            'customer'     => !empty($customer) ? $customer : '',
            'driver'       => !empty($driver) ? $driver : '',
            'destination'  => !empty($destination) ? $destination : '',
            'cargo'        => !empty($cargo) ? $cargo : '',
            'start'        => $start,
            'end'          => $end,
        ];

        foreach ($setups as $parameter => $value) {
            $setup = Setting::where('profile', '=', 'search')->where('parameter', '=', $parameter)->first();

            if(!empty($setup)) {
                $setup->update([ 'value' => $value ]);
            } else {
                $setup = Setting::create([ 'profile' => 'search', 'parameter' => $parameter, 'value' => $value ]);
            }
        }





        // Secondly, search and return requested data
        $logs = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'logs.id', 
                    'logs.direction', 
                    'logs.driver', 
                    'logs.cargo', 
                    'logs.customer', 
                    'logs.destination', 
                    'logs.comment', 
                    'logs.created_at', 
                    'permits.number', 
                    'emitters.emitter', 
                    'owners.owner', 
                    'cars.plate', 
                    'cars.model'
                )
            ->when($direction, function ($query, $direction) {
                if (count($direction) == 1) {
                    return $query->where('direction', '=', $direction[0]);
                }
            })
            ->when($driver, function ($query, $driver) {
                return $query->where('driver', 'LIKE', '%' . $driver . '%');
            })
            ->when($cargo, function ($query, $cargo) {
                return $query->where('cargo', 'LIKE', '%' . $cargo . '%');
            })
            ->when($customer, function ($query, $customer) {
                return $query->where('customer', 'LIKE', '%' . $customer . '%');
            })
            ->when($destination, function ($query, $destination) {
                return $query->where('destination', 'LIKE', '%' . $destination . '%');
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('logs.created_at', '>=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('logs.created_at', '<=', $end);
            })
            ->when($export, function ($query) {
                return $query->orderBy('logs.created_at', 'ASC');
            })
            ->when(!$export, function ($query) {
                return $query->orderBy('logs.created_at', 'DESC');
            })
            ->when($offset, function ($query, $offset) {
                return $query->skip($offset);
            })
            ->when($rows, function ($query, $rows) {
                return $query->take($rows);
            })
            ->get();


        if($export) {
            $border = (new BorderBuilder())
                ->setBorderTop(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderRight(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderBottom(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->setBorderLeft(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
                ->build();
            
            $defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->build();

            $styleHeader = (new StyleBuilder())
                ->setBorder($border)
                ->setFontBold()
                // ->setShouldWrapText()
                ->setFontColor(Color::BLUE)
                ->setBackgroundColor(Color::YELLOW)
                ->build();

            $styleRow = (new StyleBuilder())
                ->setBorder($border)
                ->build();

            $writer = WriterFactory::create(Type::ODS);
            $writer->setShouldCreateNewSheetsAutomatically(true);
            $writer->setDefaultRowStyle($defaultStyle)->openToBrowser('export.ods');

            $writer->addRowWithStyle([
                'Направление', 
                'Гос. номер', 
                'Марка авто', 
                'Владелец авто', 
                'Номер пропуска', 
                'Эмитент', 
                'Дата и время', 
                'Водитель', 
                'Груз', 
                'Заказчик', 
                'Пункт назначения', 
                'Примечание', 
            ], $styleHeader);

            foreach($logs as $item) {
                $writer->addRowWithStyle([
                    $item->direction,
                    $item->plate,
                    $item->model,
                    $item->owner,
                    (int)$item->number,
                    $item->emitter,
                    $item->created_at,
                    $item->driver,
                    $item->cargo,
                    $item->customer,
                    $item->destination,
                    $item->comment,
                ], $styleRow); // add a row at a time
            }

            $writer->close();


            return $writer->getSheets();
        }

        $totalRows = DB::table('logs')
            ->join('permits', 'logs.permits_id', '=', 'permits.id')
            ->join('emitters', 'permits.emitters_id', '=', 'emitters.id')
            ->join('owners', 'permits.owners_id', '=', 'owners.id')
            ->join('cars', 'permits.cars_id', '=', 'cars.id')
            ->select(
                    'logs.id', 
                    'logs.direction', 
                    'logs.driver', 
                    'logs.cargo', 
                    'logs.customer', 
                    'logs.destination', 
                    'logs.comment', 
                    'logs.created_at', 
                    'permits.number', 
                    'emitters.emitter', 
                    'owners.owner', 
                    'cars.plate', 
                    'cars.model'
                )
            ->when($direction, function ($query, $direction) {
                if (count($direction) == 1) {
                    return $query->where('direction', '=', $direction[0]);
                }
            })
            ->when($driver, function ($query, $driver) {
                return $query->where('driver', 'LIKE', '%' . $driver . '%');
            })
            ->when($cargo, function ($query, $cargo) {
                return $query->where('cargo', 'LIKE', '%' . $cargo . '%');
            })
            ->when($customer, function ($query, $customer) {
                return $query->where('customer', 'LIKE', '%' . $customer . '%');
            })
            ->when($destination, function ($query, $destination) {
                return $query->where('destination', 'LIKE', '%' . $destination . '%');
            })
            ->when($emitters, function ($query, $emitters) {
                return $query->whereIn('emitter', $emitters);
            })
            ->when($plate, function ($query, $plate) {
                return $query->where('plate', 'LIKE', '%' . $plate . '%');
            })
            ->when($model, function ($query, $model) {
                return $query->where('model', 'LIKE', '%' . $model . '%');
            })
            ->when($owner, function ($query, $owner) {
                return $query->where('owner', 'LIKE', '%' . $owner . '%');
            })
            ->when($start, function ($query, $start) {
                return $query->where('logs.created_at', '>=', $start);
            })
            ->when($end, function ($query, $end) {
                return $query->where('logs.created_at', '<=', $end);
            })
            ->count();


        return [ 'totalRows' => $totalRows, 'logs' => $logs ];
    }




}
