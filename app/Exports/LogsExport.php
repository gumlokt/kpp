<?php

namespace App\Exports;

use App\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogsExport implements FromCollection, ShouldAutoSize
{
    private $data = array([
        'Направление', 
        'Гос. номер', 
        'Марка авто', 
        'Владелец авто', 
        'Номер пропуска', 
        'Эмитент', 
        'Дата и время', 
        'Водитель', 
        'Груз', 
        'Заказчик', 
        'Пункт назначения', 
        'Примечание', 
    ]);

    public function __construct($logs) {
        foreach($logs as $item) {
            $this->data[] = [
                'Направление'  => $item->direction,
                'Гос. номер'   => $item->plate,
                'Марка авто'    => $item->model,
                'Владелец авто'  => $item->owner,
                'Номер пропуска'   => $item->number,
                'Эмитент' => $item->emitter,
                'Дата и время' => $item->created_at,
                'Водитель' => $item->driver,
                'Груз' => $item->cargo,
                'Заказчик' => $item->customer,
                'Пункт назначения' => $item->destination,
                'Примечание' => $item->comment,
            ];
        }
    }



    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection() {
        return collect($this->data);
        // return Log::all();
    }
}
