<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('permits', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('emitters_id')->unsigned()->index();
            $table->integer('owners_id')->unsigned()->index();
            $table->integer('cars_id')->unsigned()->index();

            $table->string('number')->index();

            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('permits');
    }
}
