import moment from "moment";
export default {
    state: {
        permitActionPath: "/permit/store",
        permitHeader: "Добавить новый пропуск",

        permits: [],

        categories: [],
        emitters: [],

        permitFilteringPath: "/permits/getvalid",
        totalRows: 0,
        rowsRoDisplay: 30, // if this variable equals 0 rows (or false), then it is used for exporting logs to Excel and means "fetch all appropriate rows"
        permitsPage: 1,
        pages: 1,

        enterDatesManually: 1,

        permitData: {
            id: "",
            number: "",
            weapon: false,
            privileged: false,
            owner: "",
            model: "",
            plate: "",
            start: "",
            end: "",
            emitter: "",
            abbr: "",
        },

        permitSuggestions: {
            number: [],
            owner: [],
            model: [],
            plate: [],
        },
    },

    getters: {
        permits: function (state) {
            return state.permits;
        },
    },

    mutations: {
        populatePermits: function (state, payload) {
            axios
                .post(state.permitFilteringPath, {
                    filters: {
                        categories: state.categories,
                        emitters: state.emitters,

                        plate: state.permitData.plate,
                        model: state.permitData.model,
                        owner: state.permitData.owner,

                        number: state.permitData.number,
                        weapon: state.permitData.weapon,
                        privileged: state.permitData.privileged,
                        start: state.permitData.start,
                        end: state.permitData.end,
                    },
                    page: state.permitsPage,
                    rows: state.rowsRoDisplay,
                })
                .then((response) => {
                    state.totalRows = +response.data.totalRows; // + converts data from string to number

                    if (state.totalRows > state.rowsRoDisplay) {
                        state.pages = Math.floor(
                            state.totalRows / state.rowsRoDisplay
                        );

                        if (state.totalRows % state.rowsRoDisplay) {
                            state.pages++;
                        }
                    } else {
                        state.pages = 1;
                    }

                    state.permits = response.data.permits;
                });
        },

        updatePermitData: function (state, payload) {
            // [:print:] matches a visible character [\x21-\x7E]
            // \s+ matches any whitespace character (equal to [\r\n\t\f\v ])
            var pattern =
                /[\d\w\s\+\-\*\/~!@"#№$;%\^:&?<>\(\)\[\]\{\}а-яА-ЯёЁ]+/;

            if (pattern.test(payload.value)) {
                if (!/^\s+/.test(payload.value)) {
                    state.permitData[payload.field] = payload.value;
                }
                // console.log('field: ' + payload.field + '; value: ' + payload.value);
            } else {
                state.permitData[payload.field] = "";
            }
        },

        setPermitsPage: function (state, payload) {
            state.permitsPage = payload.page;
        },

        setPermitFilteringPath: function (state, payload) {
            state.permitFilteringPath = payload;
        },

        setCategories: function (state, payload) {
            state.categories = payload;
        },

        setPermitEmitters: function (state, payload) {
            state.emitters = payload;
        },

        updatePermitSuggestions: function (state, payload) {
            state.permitSuggestions[payload.field] = payload.value;
        },

        clearPermitSuggestions: function (state) {
            state.permitSuggestions = {
                number: [],
                owner: [],
                model: [],
                plate: [],
            };
        },

        clearPermitData: function (state) {
            state.permitData = {
                id: "",
                number: "",
                weapon: false,
                privileged: false,
                owner: "",
                model: "",
                plate: "",
                start: "",
                end: "",
                emitter: "",
                abbr: "",
            };
            state.permitActionPath = "/permit/store";
            state.permitHeader = "Добавить новый пропуск";
        },

        addPermit: function (state, payload) {
            state.permitActionPath = "/permit/store";
            state.permitHeader = "Добавить новый пропуск";

            axios.get("/permit/" + payload + "/edit").then((response) => {
                var result = response.data[0];

                result.start = moment(new Date(result.start)).format(
                    "DD.MM.YYYY"
                );
                result.end = moment(new Date(result.end)).format("DD.MM.YYYY");

                state.permitData = result;
                state.permitData.number = "";
                state.permitData.start = "";
                state.permitData.end = "";
            });
        },

        editPermit: function (state, payload) {
            state.permitActionPath = "/permit/update";
            state.permitHeader = "Отредактировать пропуск";

            axios.get("/permit/" + payload + "/edit").then((response) => {
                var result = response.data[0];

                result.weapon
                    ? (result.weapon = true)
                    : (result.weapon = false);

                result.privileged
                    ? (result.privileged = true)
                    : (result.privileged = false);

                result.start = moment(new Date(result.start)).format(
                    "DD.MM.YYYY"
                );
                result.end = moment(new Date(result.end)).format("DD.MM.YYYY");

                state.permitData = result;
            });
        },

        invalidatePermit: function (state, payload) {
            state.permitActionPath = "/permit/" + payload + "/invalidate";
            state.permitHeader = "Аннулировать пропуск";

            axios.get("/permit/" + payload + "/edit").then((response) => {
                var result = response.data[0];

                result.start = moment(new Date(result.start)).format(
                    "DD.MM.YYYY"
                );
                result.end = moment(new Date(result.end)).format("DD.MM.YYYY");

                state.permitData = result;
            });
        },
    },

    actions: {},
};
