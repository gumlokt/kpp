import Registration from '../components/Registration/Registration';
import Permits from '../components/Permits/Permits';
import Search from '../components/Search/Search';
import Login from '../components/Login/Login';
import Settings from '../components/Settings/Settings';


export const routes = [
    { path: '/', component: Registration },
    { path: '/permits', component: Permits },
    { path: '/search', component: Search },
    { path: '/login', component: Login },
    { path: '/settings', component: Settings },
    // { path: '*', redirect: to => { return '/' }}
];
