@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <h5 class="card-header text-danger">
                    <i class="fa fa-database"></i> Бэкап Базы Данных
                </h5>

                <div class="card-body">
                    <div class="row">
                        <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="input-group">
                                <button type="button" class="btn btn-outline-success" data-toggle="tooltip" data-placement="top" data-html="true" title="Создать бэкап базы данных" v-on:click="dumpDB()" v-bind:disabled="btnStatus">
                                    <i class="fas fa-play fa-fw" v-if="!animateDumping"></i> <i class="fas fa-spinner fa-pulse fa-fw" v-if="animateDumping"></i> Создать бэкап БД <span class="text-secondary" v-show="timeElapsed">@{{ timeElapsed }} ...</span>
                                </button>
                            </div>
                        </div>
                        <!-- <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <transition name="fade">
                                <span v-if="true">some text</span>
                            </transition>
                        </div> -->
                    </div>
                </div>

                <div class="row" v-if="files">
                    <div class="col">
                        <table class="table table-hover">
                            <tbody>
                                <tr v-for="(file, index) in files" v-bind:class="[curIndex == index ? 'table-warning' : '']">
                                    <td>
                                        <span v-bind:class="[curIndex == index ? 'text-danger' : '']">@{{ file }}</span>
                                    </td>

                                    <td class="text-right">
                                        <button type="button" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="Скачать бэкап базы данных" v-on:click="downloadFile(index)">
                                            <i class="fas fa-download fa-fw"></i> Скачать бэкап БД
                                        </button>

                                        <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-placement="top" data-html="true" title="Скачать бэкап базы данных" v-on:click="deleteFile(index)" v-bind:disabled="btnStatus">
                                            <i class="fas fa-times fa-fw" v-if="curIndex == index ? false : true"></i> <i class="fas fa-spinner fa-pulse fa-fw" v-if="curIndex == index ? true : false"></i> Удалить бэкап БД
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('customJs')
<script>
        window.axios.defaults.headers.common = {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        };

        var app = new Vue({
            el: '#adminPanel',
            data: {
                animateDumping: false,
                btnStatus: false,
                timeElapsed: 0,
                interval: null,
                files: null,
                curIndex: -1
            },
            methods: {
                startStopwatch: function () {
                    this.timeElapsed = 1;
                    this.interval = setInterval(() => { this.timeElapsed++; }, 1000);
                },
                stopStopwatch: function () {
                    clearInterval(this.interval);
                    this.timeElapsed = 0;
                    this.btnStatus = false;
                    this.animateDumping = false;
                },
                displayFiles: function () {
                    axios.get('/adminpanel/displayFiles').then(response => {
                        this.files = response.data.files;
                        this.curIndex = -1;
                        this.btnStatus = false;
                        console.log(response.data);
                    }).catch(error => {
                        console.log(error);
                    });
                },
                dumpDB: function () {
                    this.btnStatus = true;
                    this.animateDumping = true;
                    this.startStopwatch();

                    axios.get('/adminpanel/dumpdb').then(response => {
                        this.displayFiles();
                        this.btnStatus = false;
                        this.animateDumping = false;
                        this.stopStopwatch();
                        console.log(response.data);
                    }).catch(error => {
                        this.stopStopwatch();
                        console.log(error);
                    });
                },
                downloadFile: function (index) {
                    const url = '/adminpanel/downloadFile/' + this.files[index];
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', this.files[index]); //or any other extension
                    document.body.appendChild(link);
                    link.click();
                },
                deleteFile: function (index) {
                    this.btnStatus = true;
                    this.curIndex = index;

                    axios.post('/adminpanel/deleteFile', {
                        fileName: this.files[index]
                    }).then(response => {
                        this.displayFiles();
                        console.log(response.data);
                    }).catch(error => {
                        this.displayFiles();
                        console.log(error);
                    });
                }
            },
            created: function () {
                this.displayFiles();
            }
        })
    </script>
@endsection
