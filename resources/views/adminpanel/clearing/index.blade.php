@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <h5 class="card-header text-danger">
                    <i class="fa fa-broom"></i> Очистка Базы Данных
                </h5>

                <div class="card-body">
                    <div class="row">
                        <div class="col-8 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                            <div class="input-group mb-3">
                                <flat-pickr v-model="date" :config="config" placeholder="Задайте дату" name="birthdate" v-bind:disabled="btnStatus"></flat-pickr>
                                <div class="input-group-append">
                                    <span class="clear-input-field input-group-text" v-on:click="clearField()">
                                        <i class="fas fa-times"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="input-group">
                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-placement="top" data-html="true" title="Очистить базу данных" v-on:click="clearDB()" v-bind:disabled="!checkDate">
                                    <i class="fas fa-times" v-if="!btnStatus"></i> <i class="fas fa-spinner fa-pulse" v-if="btnStatus"></i> Очистить БД
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="row" v-show="status">
                        <div class="col">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <div class="row">
                                    <div class="col-1">
                                        <span v-show="timeElapsed">@{{ timeElapsed }}...</span>
                                    </div>
                                    <div class="col-11">
                                        <span v-show="status">@{{ status }}</span>
                                    </div>
                                </div>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" v-on:click="clearStatus()">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div><strong class="text-danger">*</strong> <em> Поскольку очистка база данных является необратимой процедурой (после которой станет невозможным восстановление очищенных данных), перед очисткой рекомендуется выполнить резервное копирование данных.</em></div>
                    <div><strong class="text-danger">*</strong> <em> В результате очистки, из базы данных будут удалены все пропуска, срок действия которых истёк на указанную дату, а также все записи журнала въезда-выезда, в которых участвовали удаляемые пропуска.</em></div>
                    <div><strong class="text-danger">*</strong> <em> В зависимости от размера базы данных, процедура очистки может занять продолжительное время. В течение этого времени нельзя пользоваться приложением.</em></div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('customJs')
<script>
        window.axios.defaults.headers.common = {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        };

        Vue.component('flat-pickr', VueFlatpickr);

        var app = new Vue({
            el: '#adminPanel',
            data: {
                date: null,
                config: {
                    dateFormat: "d.m.Y",
                    altFormat: "d.m.Y",
                    altInput: true,
                    maxDate: ('0'+((new Date()).getDate() - 1)).slice(-2) + '.' + ('0'+((new Date()).getMonth() + 1)).slice(-2) + '.' + (new Date()).getFullYear()
                },
                btnStatus: false,
                status: null,
                timeElapsed: 0,
                interval: null
            },
            methods: {
                clearField: function () {
                    this.date = null;
                },

                clearStatus: function () {
                    this.status = null;
                },

                startStopwatch: function () {
                    this.timeElapsed = 1;
                    this.interval = setInterval(() => { this.timeElapsed++; }, 1000);
                },

                stopStopwatch: function () {
                    clearInterval(this.interval);
                    this.clearField();
                    this.timeElapsed = 0;
                    this.btnStatus = false;
                },

                clearDB: function () {
                    this.startStopwatch();
                    this.status = 'Запущен процесс очистки базы данных. Дождитесь завершения...';
                    this.btnStatus = true;

                    axios.post('/adminpanel/cleardb', {
                        date: this.date
                    }).then(response => {
                        console.log(response.data);

                        this.status = response.data.message;
                        this.stopStopwatch();
                    }).catch(error => {
                        console.log(error);
                        this.stopStopwatch();
                    });
                },

                dumpDB: function () {
                    axios.get('/adminpanel/dumpdb').then(response => {
                        console.log(response.data);
                    }).catch(error => {
                        console.log(error);
                    });
                }
            },
            computed: {
                checkDate: function () {
                    // var datePattern = /^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/; // любая дата в формате дд.мм.гггг
                    var datePattern = /^[0-9]{2}\.[0-9]{2}\.20[1-9]{1}[0-9]{1}$/; // любая дата от 2010 года либо более

                    if (datePattern.test(this.date) && !this.btnStatus) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        })
    </script>
@endsection
