@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    <h5 class="text-danger">Удаление</h5>
                </div>

                <div class="card-body">
                    @if (isset($procurement))
                        <form action="{{ url('adminpanel/procurment/' . $procurement->id . '/destroy') }}" method="POST">
                            {{ csrf_field() }}


                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><i class="fa fa-hashtag"></i> Номер</label>
                                <div class="col-sm-8">
                                    {{ $procurement->number }}
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><i class="fa fa-heading"></i> Наименование</label>
                                <div class="col-sm-8">
                                    {{ $procurement->title }}
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><i class="fa fa-comment"></i> Примечание</label>
                                <div class="col-sm-8">
                                    {{ $procurement->comment }}
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><i class="fa fa-folder"></i> Файлы</label>
                                <div class="col-sm-8">
                                    @if ($procurement->announcement)
                                        <i class="fa fa-file-pdf text-danger"></i> <a href="{{ url('adminpanel/download/announcement/' . $procurement->id) }}">Извещение</a>
                                    @endif

                                    @if ($procurement->documentation)
                                        &emsp;<i class="fa fa-file-word text-primary"></i> <a href="{{ url('adminpanel/download/documentation/' . $procurement->id) }}">Документация</a>
                                    @endif
                                </div>
                            </div>
                            <hr>




                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <span class="text-danger"><strong>Внимание!</strong></span> <span class="text-muted">Все записи в базе данных будут стёрты (включая файлы документации) без возможности последующего восстановления</span>
                                </div>
                            </div>
                            <br>


                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-outline-danger"><i class="fa fa-times"></i> Удалить</button>
                                    <button type="submit" class="btn btn-outline-primary" formaction="{{ url()->previous() }}" formmethod="POST"><i class="fa fa-long-arrow-alt-left"></i> Отменить</button>
                                </div>
                            </div>

                        </form>
                    @endif


                    @if (isset($procurements))
                        <form action="{{ url('adminpanel/archives/destroyall') }}" method="POST">
                            {{ csrf_field() }}


                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <h5><span class="text-danger">Внимание!</span> Вы уверены в том, что хотите удалить все архивные закупки?</h5>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <span class="text-muted">Все записи в базе данных будут стёрты (включая прилагаемые файлы) без возможности последующего восстановления</span>
                                </div>
                            </div>
                            <br>

                            <div class="form-group row">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-outline-danger"><i class="fa fa-times"></i> Удалить</button>
                                    <button type="submit" class="btn btn-outline-primary" formaction="{{ url()->previous() }}" formmethod="POST"><i class="fa fa-long-arrow-alt-left"></i> Отменить</button>
                                </div>
                            </div>

                        </form>
                    @endif
                </div>
            </div>

        </div>
    </div>






@endsection
