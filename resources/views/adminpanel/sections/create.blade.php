@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    @if (isset($procurement))
                        <h5 class="text-danger">Редактировать закупку (лот)</h5>
                    @else
                        <h5 class="text-danger">Создать новую закупку (лот)</h5>
                    @endif
                </div>

                <div class="card-body">
                    @if (isset($procurement))
                        <form action="{{ url('adminpanel/procurment/' . $procurement->id . '/update') }}" method="POST" enctype="multipart/form-data">
                    @else
                        <form action="{{ url('adminpanel/procurment/store') }}" method="POST" enctype="multipart/form-data">
                    @endif
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><i class="fa fa-hashtag"></i> Номер</label>
                            <div class="col-sm-8">
                                @if (isset($procurement))
                                    <input type="text" class="form-control" name="number" placeholder="Номер закупки (лота)" value="{{ $procurement->number }}">
                                @else
                                    <input type="text" class="form-control" name="number" placeholder="Номер закупки (лота)" value="{{ old('number') }}">
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><i class="fa fa-heading"></i> Наименование</label>
                            <div class="col-sm-8">
                                @if (isset($procurement))
                                    <textarea class="form-control" name="title" rows="2">{{ $procurement->title }}</textarea>
                                @else
                                    <textarea class="form-control" name="title" rows="2">{{ old('title') }}</textarea>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><i class="fa fa-comment"></i> Примечание</label>
                            <div class="col-sm-8">
                                @if (isset($procurement))
                                    <textarea class="form-control" name="comment" rows="2">{{ $procurement->comment }}</textarea>
                                @else
                                    <textarea class="form-control" name="comment" rows="2">{{ old('comment') }}</textarea>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><i class="fa fa-calendar-alt"></i> Период</label>
                            <div class="col-sm-3">
                                @if (isset($procurement))
                                    <input type="date" class="form-control" name="dateStart" placeholder="Начало подачи заявок" value="{{ $procurement->dateStart }}" id="dateStart">
                                @else
                                    <input type="date" class="form-control" name="dateStart" placeholder="Начало подачи заявок" value="{{ old('dateStart') }}" id="dateStart">
                                @endif
                            </div>
                            <div class="col-sm-3">
                                @if (isset($procurement))
                                    <input type="date" class="form-control" name="dateEnd" placeholder="Окончание подачи заявок" value="{{ $procurement->dateEnd }}" id="dateEnd">
                                @else
                                    <input type="date" class="form-control" name="dateEnd" placeholder="Окончание подачи заявок" value="{{ old('dateEnd') }}" id="dateEnd">
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <small><strong class="text-danger">*</strong> <strong>[</strong> <span class="text-info">ГГГГ-ММ-ДД ЧЧ:ММ</span> <strong>]</strong></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><i class="fa fa-file-pdf"></i> Извещение</label>
                            <div class="col-sm-4">
                                @if (isset($files) and $files['announcement'])
                                    <a href="{{ url('adminpanel/download/announcement/' . $procurement->id) }}"><i class="fa fa-download"></i> Скачать</a> | 
                                    <a href="{{ url('adminpanel/delete/announcement/' . $procurement->id) }}" class="text-danger"><i class="fa fa-times"></i> Удалить</a> <i class="fa fa-question-circle text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Удаление файла является необратимой операцией. Но после неё можно будет прикрепить другой файл."></i>
                                @else
                                    <label class="custom-file">
                                        <input type="file" name="announcement" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label>
                                @endif
                            </div>
                            <div class="col-sm-4">
                                @if (session('announcement'))
                                    <div class="alert alert-success alert-dismissible fade show fadingMessage" role="alert" style="margin: 0; padding: 5px 8px;">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0; padding: 8px 16px 0px;">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        {{ session('announcement') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><i class="fa fa-file-archive"></i> Документация</label>
                            <div class="col-sm-4">
                                @if (isset($files) and $files['documentation'])
                                    <a href="{{ url('adminpanel/download/documentation/' . $procurement->id) }}"><i class="fa fa-download"></i> Скачать</a> | 
                                    <a href="{{ url('adminpanel/delete/documentation/' . $procurement->id) }}" class="text-danger"><i class="fa fa-times"></i> Удалить</a> <i class="fa fa-question-circle text-muted" data-toggle="tooltip" data-placement="right" data-html="true" title="Удаление файла является необратимой операцией. Но после неё можно будет прикрепить другой файл."></i>
                                @else
                                    <label class="custom-file">
                                        <input type="file" name="documentation" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label>
                                @endif
                            </div>
                            <div class="col-sm-4">
                                @if (session('documentation'))
                                    <div class="alert alert-success alert-dismissible fade show fadingMessage" role="alert" style="margin: 0; padding: 5px 8px;">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0; padding: 8px 16px 0px;">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        {{ session('documentation') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"><i class="fa fa-circle"></i> Статус</label>
                            <div class="col-sm-8">
                                <select class="custom-select" name="status">
                                    @if (isset($procurement))
                                        <option value="draft"{{ 'draft' == $procurement->status ? ' selected' : '' }}>Черновик</option>
                                        <option value="actual"{{ 'actual' == $procurement->status ? ' selected' : '' }}>Опубликовано</option>
                                    @else
                                        <option value="draft">Черновик</option>
                                        <option value="actual">Опубликовано</option>
                                    @endif

                                    @if (isset($procurement) and 'archive' == $procurement->status)
                                        <option value="archive" selected="">Архив</option>
                                    @endif
                                </select>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="offset-sm-4 col-sm-8">
                                @if (isset($procurement))
                                    <button type="submit" class="btn btn-outline-success"><i class="fa fa-floppy"></i> Сохранить</button>
                                @else
                                    <button type="submit" class="btn btn-outline-success"><i class="fa fa-check"></i> Создать</button>
                                @endif
                                    {{-- <button type="submit" class="btn btn-outline-primary" formaction="{{ url()->previous() }}" formmethod="POST"><i class="fa fa-long-arrow-alt-left"></i> Вернуться назад</button> --}}
                            </div>
                        </div>

                    </form>
                </div>
            </div>
{{--             @if (isset($files) and !empty($files))
                {{ dump($files) }}
            @endif --}}

        </div>
    </div>






@endsection
