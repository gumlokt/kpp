                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th width="12%">Пользователь</th>

                                    <th width="11%">№ закупки</th>
                                    <th width="36%">Наименование</th>
                                    <th>Примечание</th>

                                    <th width="16%">Файлы</th>
                                    <th width="10%">Начало</th>
                                    <th width="10%">Окончание</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($procurements as $procurement)
                                    <tr class="procurement">
                                        <td>{{ $procurement->updaters->name }}</td>

                                        <td>{{ $procurement->number }}</td>
                                        <td>{{ $procurement->title }}</td>
                                        <td>{{ $procurement->comment }}</td>

                                        <td>
                                            @if ($procurement->announcement)
                                                <i class="fa fa-file-pdf-o text-danger"></i> <a href="{{ url('adminpanel/download/announcement/' . $procurement->id) }}">Извещение</a>
                                                <br>
                                            @endif

                                            @if ($procurement->documentation)
                                                <i class="fa fa-file-word-o text-primary"></i> <a href="{{ url('adminpanel/download/documentation/' . $procurement->id) }}">Документация</a>
                                            @endif
                                        </td>

                                        <td>{{ $procurement->dateStart }}</td>
                                        <td>
                                            {{ $procurement->dateEnd }}

                                            <form class="form-inline">
                                                @if('draft' == $procurement->status)
                                                    <button class="btn btn-secondary btn-sm" type="submit" formaction="{{ url('/adminpanel/procurment/' . $procurement->id . '/activate') }}" formmethod="GET" title="Опубликовать закупку"><i class="fa fa-play"></i></button>
                                                @else
                                                    <button class="btn btn-success btn-sm" type="submit" formaction="{{ url('/adminpanel/procurment/' . $procurement->id . '/draft') }}" formmethod="GET" title="Изменить статус закупки на 'Черновик'"><i class="fa fa-pause"></i></button>
                                                @endif

                                                    <button class="btn btn-warning btn-sm" type="submit" formaction="{{ url('/adminpanel/procurment/' . $procurement->id . '/edit') }}" formmethod="GET" title="Редактировать закупку"><i class="fa fa-pencil-alt"></i></button>

                                                @if('archive' == $procurement->status)
                                                    <button class="btn btn-primary btn-sm" type="submit" formaction="{{ url('/adminpanel/procurment/' . $procurement->id . '/archive') }}" formmethod="GET" title="Переместить закупку в архив (историю)" disabled><i class="fa fa-trash-alt"></i></button>
                                                @else
                                                    <button class="btn btn-primary btn-sm" type="submit" formaction="{{ url('/adminpanel/procurment/' . $procurement->id . '/archive') }}" formmethod="GET" title="Переместить закупку в архив (историю)"><i class="fa fa-trash-alt"></i></button>
                                                @endif

                                                    <button class="btn btn-danger btn-sm" type="submit" formaction="{{ url('/adminpanel/procurment/' . $procurement->id . '/destroy') }}" formmethod="GET" title="Удалить закупку (без возможности восстановления)"><i class="fa fa-times"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
