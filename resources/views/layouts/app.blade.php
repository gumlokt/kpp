<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Регистрация движения автотранспорта через КПП">
    <meta name="author" content="Kakupshev Igor Vladimirovich">
    <link rel="shortcut icon" href="{{ url('ico/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Пургаз</title>

    <link href="{{ url('css/app.css') }}" rel="stylesheet" type="text/css">

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(), ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <div class="container">
            @yield('content')
        </div>
    </div>

    <script src="{{ url('js/app.js') }}"></script>
</body>
</html>
