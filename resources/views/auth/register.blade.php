@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-6">

        <div class="card">
            <div class="card-header">
                <h5 class="text-center text-danger">Регистрация пользователя</h5>
            </div>

            <div class="card-body">
                <form action="{{ route('register') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <label for="name" class="col-sm-4 col-form-label"><i class="fa fa-user"></i> Имя</label>
                        <div class="col-sm-8">
                            <input id="name" type="text" class="form-control" name="name" placeholder="Ваше имя" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('name') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('email') ? ' has-danger' : '' }}">
                        <label for="email" class="col-sm-4 col-form-label"><i class="fa fa-envelope"></i> E-Mail</label>
                        <div class="col-sm-8">
                            <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('email') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('password') ? ' has-danger' : '' }}">
                        <label for="password" class="col-sm-4 col-form-label"><i class="fa fa-key"></i> Пароль</label>
                        <div class="col-sm-8">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Пароль" required>

                            @if ($errors->has('password'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('password') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                        <label for="password-confirm" class="col-sm-4 col-form-label"><i class="fa fa-key"></i> Подтверждение</label>
                        <div class="col-sm-8">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Подтверждение пароля" required>

                            @if ($errors->has('password_confirmation'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('password_confirmation') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-4 col-sm-8">
                            <button type="submit" class="btn btn-secondary"><i class="fa fa-check"></i> Зарегистрироваться</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>






@endsection
