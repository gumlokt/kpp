<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();




// --- admin section ---
// ------------------------------------------------------------------------------------
Route::any('adminpanel', 'AdminController@index');


// --- adminpanel --- Emitters
Route::get('adminpanel/emitters', 'EmitterController@index');
Route::get('adminpanel/emitter/create', 'EmitterController@create');
Route::post('adminpanel/emitter/store', 'EmitterController@store');

Route::get('adminpanel/emitter/{id}/edit', 'EmitterController@edit');
Route::post('adminpanel/emitter/{id}/update', 'EmitterController@update');

Route::get('adminpanel/emitter/{id}/pause', 'EmitterController@pause');
Route::get('adminpanel/emitter/{id}/activate', 'EmitterController@activate');

// --- adminpanel --- Сlearing
Route::get('adminpanel/clearing', 'AdminController@showClearing');
Route::post('adminpanel/cleardb', 'AdminController@cleardb');

// --- adminpanel --- Make, download and delete DB dumps
Route::get('adminpanel/backups', 'AdminController@showBackups');
Route::get('adminpanel/dumpdb', 'AdminController@dumpdb');
Route::get('adminpanel/displayFiles', 'AdminController@displayFiles');
Route::get('adminpanel/downloadFile/{fileName}', 'AdminController@downloadFile');
Route::post('adminpanel/deleteFile', 'AdminController@deleteFile');


// --- adminpanel --- Web-application settings
// Route::get('adminpanel/settings/show', 'AdminController@show');
// Route::post('adminpanel/settings/save', 'AdminController@save');





// --- admin section ---
// -----------------------------------------------------------------------------------------------------------//
// Emitters
Route::get('emitters/actual', 'AjaxController@actualEmitters');



// Logs
Route::post('logs/last', 'LogController@lastLogs');
Route::post('logs/plate', 'LogController@plateLogs');
// Route::get('logs/page/{number}', 'LogController@pageLogs');

Route::get('logs/summary', 'LogController@summary');

Route::post('logs/field/autocomplete', 'LogController@fieldAutocomplete');

Route::post('log/register/{direction}', 'LogController@register');
Route::get('log/{id}/reverse', 'LogController@reverse');



// Permits create/edit actions
Route::get('permits/plate/autocomplete', 'PermitController@plateAutocomplete');
Route::post('permits/field/autocomplete', 'PermitController@fieldAutocomplete');

Route::get('permits/summary', 'PermitController@summary');
Route::post('permits/getvalid', 'PermitController@validpermits');

Route::get('permit/{id}/edit', 'PermitController@edit');
Route::get('permit/{id}/invalidate', 'PermitController@invalidate');

Route::post('permit/store', 'PermitController@store');
Route::post('permit/update', 'PermitController@update');



// Filtering
Route::post('filtering/permits', 'AjaxController@filterPermits');
Route::post('filtering/search', 'AjaxController@filterSearch');



// Settings
Route::get('settings/permits', 'SettingController@permitsFilters');
Route::get('settings/search', 'SettingController@searchFilters');
Route::get('settings/setups', 'SettingController@getSetups');
Route::post('settings/setups', 'SettingController@saveSetups');



// Export to Excel
// Route::post('export/lastlogs', 'LogController@exportLastLogs');



// All other routes
Route::get('{any}', function () {
    return view('index');
})->where('any', '.*');
